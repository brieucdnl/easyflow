﻿cmake_minimum_required (VERSION 3.8)

find_package(nlohmann_json REQUIRED)

find_package(Qt5Widgets REQUIRED)
set(CMAKE_AUTOMOC on)

include_directories(headers)

set(
	${PROJECT_NAME}_CPP
	main.cpp
	src/MainWindow.cpp
	src/ListCameraWindow.cpp
	src/Camera.cpp
	src/TreeItem.cpp
	src/TreeModel.cpp
)

set(
	${PROJECT_NAME}_H
	headers/MainWindow.h
	headers/ListCameraWindow.h
	headers/Camera.h
	headers/TreeItem.h
	headers/TreeModel.h
)

add_executable (
	${PROJECT_NAME}
	${${PROJECT_NAME}_CPP}
	${${PROJECT_NAME}_H}
)

target_link_libraries(
	${PROJECT_NAME}
	nlohmann_json::nlohmann_json
	${Qt5Widgets_LIBRARIES}
)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/config/ DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CONFIGURATION_TYPES}/config)