#include "ListCameraWindow.h"

ListCameraWindow::ListCameraWindow(QWidget* parent) : QDialog(parent) {
	setMinimumWidth(700);
	setMinimumHeight(250);
	setWindowTitle("Load Camera ...");

	auto settings(qobject_cast<MainWindow*>(this->parent())->getSettings());
	auto filePath(std::filesystem::current_path());
	filePath /= settings->value("Paths/jsonPath").toString().toStdString();
	json cameras;
	Camera::getListCamerasAvailable(filePath, cameras);

	auto layout(new QVBoxLayout(this));
	auto treeView(new QTreeView(this));
	auto modelCamera(new TreeModel(cameras, treeView));
	treeView->setSelectionMode(QAbstractItemView::SingleSelection);
	treeView->setModel(modelCamera);
	treeView->setColumnWidth(0, 280);
	treeView->setColumnWidth(1, 200);
	layout->addWidget(treeView);
	setLayout(layout);

	createPushButtons();
}

ListCameraWindow::~ListCameraWindow() {

}

void ListCameraWindow::createPushButtons() {
	auto buttonLayout(new QHBoxLayout());
	auto confirmBtn(new QPushButton(tr("Confirm"), this));
	auto cancelBtn(new QPushButton(tr("Cancel"), this));

	buttonLayout->addWidget(confirmBtn);
	buttonLayout->addWidget(cancelBtn);
	qobject_cast<QBoxLayout*>(layout())->addLayout(buttonLayout);

	connect(confirmBtn, &QPushButton::released, this, &ListCameraWindow::accept);
	connect(cancelBtn, &QPushButton::released, this, &ListCameraWindow::reject);
}

void ListCameraWindow::accept() {
	auto index(this->findChild<QTreeView*>()->selectionModel()->selectedIndexes()[0]);
	auto row(index.row());
	auto manufacturer = index.parent().sibling(0, 0).data().toString().toStdString();
	auto cameraName = index.sibling(row, 0).data().toString().toStdString();
	auto firmwareVersion = index.sibling(row, 1).data().toString().toStdString();
	auto moduleName = index.sibling(row, 2).data().toString().toStdString();
	Camera::getInstance().updateCamera(manufacturer, cameraName, firmwareVersion, moduleName);
	QDialog::accept();
}