#include "MainWindow.h"
#include <iostream>

MainWindow::MainWindow() : QMainWindow(),
							m_centralWidget(new QStackedWidget(this)),
							m_settings(new QSettings("config/config.ini",QSettings::IniFormat, this)){
	createMenus();

	auto captureViewer(new QGraphicsView());
	auto scene(new QGraphicsScene(captureViewer));
	scene->setBackgroundBrush(Qt::black);
	captureViewer->setScene(scene);
	m_centralWidget->addWidget(captureViewer);
	
	setCentralWidget(m_centralWidget);

	if (m_settings->value("Paths/jsonPath","") == "") {
		m_settings->setValue("Paths/jsonPath", "config/camera.json");
	}
}

MainWindow::~MainWindow() {

}

QSettings* MainWindow::getSettings() {
	return m_settings;
}

void MainWindow::createMenus() {
	auto menu(menuBar()->addMenu(tr("&File")));
	auto captureMenu(menuBar()->addMenu(tr("&Capture")));
	auto loadCameraAction(captureMenu->addAction(tr("Load Camera")));
	connect(loadCameraAction, &QAction::triggered, this, &MainWindow::loadCamera);
}

// ### SLOTS ###
void MainWindow::loadCamera() {
	auto loadCameraWin(new ListCameraWindow(this));
	loadCameraWin->setModal(true);
	auto ret = loadCameraWin->exec();
}