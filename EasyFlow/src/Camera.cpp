#include "Camera.h"

Camera& Camera::getInstance() {
	static Camera instance;
	return instance;
}

void Camera::getListCamerasAvailable(const std::filesystem::path& jsonPath, json& listCameras) {
	listCameras = json::parse(std::ifstream(jsonPath));
}

std::string Camera::getManufacturer() const {
	return m_manufacturer;
}

std::string Camera::getCameraName() const {
	return m_cameraName;
}

std::string Camera::getFirmwareVersion() const {
	return m_firmwareVersion;
 }

std::string Camera::getModuleName() const {
	return m_moduleName;
}

void Camera::updateCamera(const std::string manufacturer, const std::string cameraName, const std::string firmwareVersion, const std::string moduleName) {
	m_manufacturer = manufacturer;
	m_cameraName = cameraName;
	m_firmwareVersion = firmwareVersion;
	m_moduleName = moduleName;
}