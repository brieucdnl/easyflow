// STL
#include <iostream>

// Qt
#include <QApplication>

// Project
#include "MainWindow.h"
#include "Camera.h"

// Debug
#include <vld.h>

Camera::Camera() = default;
Camera::~Camera() = default;

int main(int argc, char* argv[]) {
	QApplication app(argc, argv);
	MainWindow mWin;
	mWin.showMaximized();
	return app.exec();
}