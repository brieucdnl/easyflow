#ifndef DEF_TREEITEM
#define DEF_TREEITEM

#include <QtWidgets>

class TreeItem {
	public :
		TreeItem(const QVector<QVariant>& data, TreeItem* parentITem = nullptr);
		~TreeItem();

		void appendChild(TreeItem* child);
		TreeItem* child(int row);
		int childCount() const;
		int columnCount() const;
		QVariant data(int column) const;
		int row() const;
		TreeItem* parentItem();

	private :
		QVector<TreeItem*> m_childItems;
		QVector<QVariant> m_itemData;
		TreeItem* m_parentItem;
};

#endif