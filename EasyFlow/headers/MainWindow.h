#ifndef DEF_MAINWINDOW
#define DEF_MAINWINDOW

#include <QtWidgets>
#include <QStackedWidget>

#include "ListCameraWindow.h"

class MainWindow : public QMainWindow {
	Q_OBJECT

	public :
		MainWindow();
		~MainWindow();
		QSettings* getSettings();

	private slots :
		void loadCamera();

	private	:
		QSettings* m_settings;
		QStackedWidget* m_centralWidget;

		void createMenus();
};

#endif // !DEF_MAINWINDOW
