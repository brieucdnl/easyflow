#ifndef DEF_LISTCAMERAWINDOW
#define DEF_LISTCAMERAWINDOW

#include<QtWidgets>

#include<filesystem>

#include "Camera.h"
#include "MainWindow.h"
#include "TreeModel.h"

class ListCameraWindow : public QDialog {
	Q_OBJECT

	public :
		ListCameraWindow(QWidget* parent = 0);
		~ListCameraWindow();

	private slots :
		void accept() override;

	private :
		void createPushButtons();
};

#endif