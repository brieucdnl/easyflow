#ifndef DEF_CAMERA
#define DEF_CAMERA

#include <filesystem>
#include <fstream>
#include <map>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

class Camera {
	public:
		static Camera& getInstance();

		Camera(const Camera&) = delete;
		Camera operator=(const Camera&) = delete;
		std::string getManufacturer() const;
		std::string getCameraName() const;
		std::string getFirmwareVersion() const;
		std::string getModuleName() const;
		void updateCamera(const std::string manufacturer, const std::string cameraName, const std::string firmwareVersion, const std::string moduleName);
		static void getListCamerasAvailable(const std::filesystem::path& jsonPath, json& listCameras);

	private:
		std::string m_manufacturer;
		std::string m_cameraName;
		std::string m_firmwareVersion;
		std::string m_moduleName;
		Camera();
		~Camera();
};

#endif