# EasyFlow

### Description

EasyFlow is an open-source personal software to handle from one end to the other the workflow in Photography. This software will have many applications such as :
* DSLR Remote Control
  * Photobooth
  * TimeLapse
* Image Processing Algorithms
  * HDR
  * Image Quality Enhancement
  * Filters
* File Management
* Machine Learning
  * Facial Recognition

And more to come !

### Install Requirements

Currently the project is developped on MSVC16 (2019), but Unix and MacOs may follow

1. C++ 17 : The project uses FileSystem from STL instead of using Boost
2. Json from Nlohmann ( > v3.7.3 )
   https://github.com/nlohmann/json is a library used to read json file (information about SDKs are stored this way)
3. Qt ( > v5.14.1 ) is used for the Graphical User Interface

### License

TO DO

### Author

Brieuc DANIEL - brieuc@bdaniel.fr - French Image Processing and Software Engineer graduated from �cole Sup�rieure d'Ing�nieurs de Rennes (ESIR). You may have more informations on my website : www.bdaniel.fr